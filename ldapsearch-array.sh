#!/bin/bash
###
# Copyright (c) 2018-2022 thomas.zink _at_ uni-konstanz _dot_ de
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###
# ldapsearch-array.sh
# search users against ldap given an array
# the array is read from file $1 or stdin
###

# error handling
#set -eu -o pipefail
#shopt -s failglob inherit_errexit

usage() {
    echo "usage: $(basename "$0") [file]" 
    echo "" 
    echo "Validate array of users against ldap."
    echo "If no file is given, read users from stdin."
    exit 0
}

# import library
# need absolute path for remote exec
path="$(dirname "$0")"
if [ ! -f "${path}/ldapsearch.shlib" ]; then
    echo "Error: ldapsearch.shlib not found." >&2;
    exit 1
fi
. "${path}"/ldapsearch.shlib

infile=""
if [ "$#" -ge 1 ]; then
        [[ "$1" =~ (-h|--help) ]] && { usage; }
        [ -e "$1" ] && infile="$1"
fi
[ -z "$infile" ] && echo "Info: no file found, reading from stdin (escape  ^d)" >&2

declare -a array;
mapfile -t array <"${infile:-/dev/stdin}"

# example user array:
#array=(user.1@example.com user.2@example.com)

for user in "${array[@]}"; do
   result=$(perform_ldapsearch "(mail=$user)" "dn");
   if [ -z "$result" ]; then
	   echo "no result";
   else
	   echo "$result";
   fi
done 

