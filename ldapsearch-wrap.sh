#!/bin/bash
###
# Copyright (c) 2018-2022 thomas.zink _at_ uni-konstanz _dot_ de
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###
# execute ldapsearch using a config file, default arguments, and filter/attributes
###

# error handling
#set -eu -o pipefail
#shopt -s failglob inherit_errexit

# import library
# need absolute path for remote exec
path="$(dirname "$0")"
if [ ! -f "${path}/ldapsearch.shlib" ]; then
    echo "Error: ldapsearch.shlib not found." >&2;
    exit 1
fi
. "${path}"/ldapsearch.shlib

result=$(perform_ldapsearch "$@");
echo "$result"
