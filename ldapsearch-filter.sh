#!/bin/bash
###
# Copyright (c) 2018-2022 thomas.zink _at_ uni-konstanz _dot_ de
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###
###
# ldap-search.sh
# search ldap for uid, mail, cn, and display specific attributes 
###

# error handling
#set -eu -o pipefail
#shopt -s failglob inherit_errexit

usage () {
    echo "usage: $(basename "$0") [filter]"
    echo ""
    echo "Search ldap with attribute filter."
    exit 0
}

# import library
# need absolute path for remote exec
path="$(dirname "$0")"
if [ ! -f "${path}/ldapsearch.shlib" ]; then
    echo "Error: ldapsearch.shlib not found." >&2;
    exit 1
fi
. "${path}"/ldapsearch.shlib

[ "$#" -eq "0" ] && usage
FILTER=""
case "$1" in
    "-h" | "--help")
        usage;
        ;;
    *) 
        #FILTER="$@"
        FILTER="$*"
        ;;
esac

#[ -z "${FILTER}" ] && FILTER="($FIELD=$VALUE)"
RESULT=$(perform_ldapsearch "$FILTER" ${SHOW:-\*} | grep -Ev "$IGNORE" | perl -MMIME::Base64 -n -00 -e 's/\n //g;s/(?<=:: )(\S+)/decode_base64($1)/eg;print')
echo "$RESULT"

