#!/bin/bash
###
# Copyright (c) 2018-2022 thomas.zink _at_ uni-konstanz _dot_ de
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###
###
# ldap-search.sh
# search ldap for uid, mail, cn, and display specific attributes 
###

# error handling
#set -eu -o pipefail
#shopt -s failglob inherit_errexit

usage () {
    echo "usage: $(basename "$0") [path/to/filter-file]"
    echo "Search ldap with filters provided by file."
    echo ""
    echo "filter-file:    file with filter strings separated by nl. default: stdin"
    exit 0
}

# import library
# need absolute path for remote exec
path="$(dirname "$0")"
if [ ! -f "${path}/ldapsearch.shlib" ]; then
    echo "Error: ldapsearch.shlib not found." >&2;
    exit 1
fi
. "${path}"/ldapsearch.shlib

if [[ -z "$1" ]]; then 
    >&2 echo "info: no file found, reading from stdin."
fi

while read -r line; do
    filter="${line}"
    result=$(perform_ldapsearch "${filter}" ${SHOW:-\*} | grep -Ev "$IGNORE" | perl -MMIME::Base64 -n -00 -e 's/\n //g;s/(?<=:: )(\S+)/decode_base64($1)/eg;print')
    echo "${result}"
done < "${1:-/dev/stdin}"

