#!/bin/env python
# vim: ts=4 sw=4 sts=4 noet :
# Copyright (c) 2023 tzink _at_ htwg-konstanz _dot_ de
# Usage of the works is permitted provided that this instrument is retained
# with the works, so that any entity that uses the works is notified of this
# instrument.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

import tkinter as tk
from tkinter import ttk, scrolledtext, simpledialog
import ldap3
import gnupg
import os
from xdg import BaseDirectory


def read_env(sourcefile):
    env = os.popen(". " + str(sourcefile) + " && set").read()
    env_vars = {x[:x.find("=")].strip():x[x.find("=")+1:].strip() for x in env.split("\n")[:]}
    return env_vars


class LDAPQueryApp:
    def __init__(self, root):
        self.root = root
        self.root.title("LDAP search GUI")

        self.create_gui()

        # Initialize LDAP connection
        self.ldap_connection = None

        # read config
        self.read_config()

        # Decrypt LDAP credentials using GPG
        self.load_ldap_credentials()

    def create_gui(self):
        # Make the window resizable
        self.root.resizable(True, True)
        
        # Create and place widgets
        self.filter_label = ttk.Label(self.root, text="LDAP Filter:")
        self.filter_entry = ttk.Entry(self.root, width=40)
        self.query_button = ttk.Button(self.root, text="Query", command=self.perform_ldap_query)
        self.result_text = scrolledtext.ScrolledText(self.root, wrap=tk.WORD, width=130, height=30)
        
        # Set row and column weights for resizing
        self.root.columnconfigure(1, weight=1)
        self.root.rowconfigure(1, weight=1)

        self.filter_label.grid(row=0, column=0, pady=5, padx=5, sticky=tk.W)
        self.filter_entry.grid(row=0, column=1, pady=5, padx=5, sticky=tk.W+tk.E)
        self.query_button.grid(row=0, column=2, pady=5, padx=5)
        self.result_text.grid(row=1, column=0, columnspan=3, pady=5, padx=5, sticky=tk.W+tk.E+tk.N+tk.S)

    def read_config(self):
        config = str(BaseDirectory.xdg_config_home)+"/ldapsearch/ldapsearch.conf"
        env_vars = read_env(config)
        self.ldap_uri = str(env_vars['LDAPURI'])
        self.ldap_binddn = str(env_vars['BINDDN']).strip("'")
        self.ldap_base = str(env_vars['SEARCHBASE'])
        self.ldap_passwdfile = str(BaseDirectory.xdg_config_home)+"/ldapsearch/.ldap_passwd.gpg"

    def load_ldap_credentials(self):
        #gpg_password = self.get_secure_password("Enter GPG password for LDAP credentials:")

        # Decrypt LDAP credentials using GPG
        gpg = gnupg.GPG()
        with open(self.ldap_passwdfile, "rb") as f:
            #decrypted_data = gpg.decrypt_file(f, passphrase=gpg_password)
            decrypted_data = gpg.decrypt_file(f)

        if decrypted_data.ok:
            ldap_passwd = decrypted_data.data.decode("utf-8").strip()

            # Establish LDAP connection
            self.ldap_uri = "ldap://localhost:389"
            #self.ldap_connection = ldap3.Connection("ldap://localhost:389", auto_bind=True)
            self.ldap_connection = ldap3.Connection(self.ldap_uri, auto_bind=True)

            #self.ldap_connection = ldap.initialize(self.ldap_uri)
            #self.ldap_connection.simple_bind_s(self.ldap_binddn, ldap_passwd)
        else:
            print("Failed to decrypt LDAP credentials. Exiting.")
            self.root.destroy()

    # def get_secure_password(self, prompt):
        # return simpledialog.askstring("Password", prompt, show="*")

    def perform_ldap_query(self):
        # Get LDAP filter from entry
        ldap_filter = self.filter_entry.get()

        # Perform LDAP search
        ldap3.Entry
        try:
            search_response = self.ldap_connection.search(search_base=self.ldap_base.replace("'",""), search_filter=ldap_filter, attributes=["*"])
            if search_response:
                entries = self.ldap_connection.entries

                # Display search result in the text field
                self.result_text.config(state=tk.NORMAL)
                self.result_text.delete(1.0, tk.END)
                for entry in entries:
                    s=""
                    for key,value in entry.entry_attributes_as_dict.items():
                        s += str(key) + ": " + str(value) + "\n"
                    s += "\n"
                    self.result_text.insert(tk.END, str(s) + "\n")
                self.result_text.config(state=tk.DISABLED)

        except Exception as e:
            print(f"LDAP query failed: {e}")
            raise e

if __name__ == "__main__":
    root = tk.Tk()
    app = LDAPQueryApp(root)
    root.mainloop()

