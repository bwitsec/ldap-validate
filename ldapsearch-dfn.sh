#!/bin/bash
###
# Copyright (c) 2018 thomas.zink _at_ uni-konstanz _dot_ de
# Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.
# DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
###
# validate-file.sh
# validate users by reading a file with e-mail addresses seperated by newline
###

# error handling
#set -e -o pipefail
#shopt -s failglob inherit_errexit

usage() {
    echo "usage: $(basename "$0") [file]" 
    echo "" 
    echo "Search users in ldap."
    echo "If no file is given, read users from stdin."
    echo ""
    echo "File format:"
    echo "   cn or mail|leaked-pw" 
    exit 0
}

# import library
# need absolute path for remote exec
path="$(dirname "$0")"
if [ ! -f "${path}/ldapsearch.shlib" ]; then
    echo "Error: ldapsearch.shlib not found." >&2;
    exit 1
fi
. "${path}"/ldapsearch.shlib

infile=""
if [ "$#" -ge 1 ]; then
        [[ "$1" =~ (-h|--help) ]] && { usage; }
        [ -e "$1" ] && infile="$1"
fi
[ -z "$infile" ] && echo "Info: no file found, reading from stdin (escape  ^d)" >&2

while read -r line; do
   # set user and pass if exist
   user="$line";
   pass=""
   [[ "$line" == *"|"* ]] && IFS='|' read -r user pass <<< "$line";
   # check if we got cn or mail
   if [[ "$user" == *"@"* ]]; then 
   	field="mail";
   	user="${user/uni.kn/uni-konstanz.de}" # bashism
   	#user=$(echo "$user" | sed 's/uni.kn/uni-konstanz.de/')
   else 
   	field="cn";
   fi
   result=$(perform_ldapsearch "($field=$user)" "dn");
   if [ ! -z "$result" ]; then
	   cn=$(echo -n "$result" | sed 's/.*cn=\(.*\),ou=.*/\1/');
	   mail="${cn}@uni-konstanz.de";
	   echo -n "$mail";
	   [ ! -z "$pass" ] && echo -n "|$pass";
	   echo ""
   else
	   echo "Info: no result" >&2;
   fi
done < "${infile:-/dev/stdin}"

